import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/assets/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/containers/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  darkMode: "class",
  theme: {
    extend: {
      colors: {
        bgPrimaryDark: "#111926",
        contentDark: "#0C121D",
        content: "#F3F4F6",
        primary: "#1EB141",
        bgIconHoverDark: "rgb(31,41,55)",
        bgIconHover: "rgb(243,244,246)",
        textLight: "#F9FAFB",
      },
    },
    maxWidth: {
      mobile: "90%",
      main: "1128px",
    },
  },
};
export default config;
