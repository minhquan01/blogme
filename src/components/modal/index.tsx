import { Dialog, Transition } from "@headlessui/react";
import React, { Fragment, ReactNode } from "react";

interface ModalProps {
  isOpen: boolean;
  onClose: () => void;
  title?: string;
  children: ReactNode;
  cancelText?: string;
  comfirmText?: string;
  action?: boolean;
  noBg?: boolean;
}

const Modal = ({
  isOpen,
  onClose,
  title,
  children,
  cancelText = "Huỷ",
  comfirmText = "Lưu",
  action = true,
  noBg,
}: ModalProps) => {
  return (
    <Transition appear show={isOpen}>
      <Dialog as="div" className="relative z-[99999]" onClose={onClose}>
        <Transition.Child
          enter="ease-out duration-50"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-50"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 w-full backdrop-blur-[8px] bg-opacity-25" />
        </Transition.Child>

        <div className="fixed inset-0 overflow-y-auto">
          <div
            className={`flex min-h-full  justify-center p-4 text-center relative ${
              noBg ? "items-start" : "items-center"
            }`}
          >
            <Transition.Child
              enter="ease-out duration-200"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <Dialog.Panel
                className={`${
                  !noBg && "bg-[rgb(22,28,36)] shadow-xl"
                } w-fit relative h-fit transform overflow-hidden dark:text-white rounded-2xl px-6 py-10 text-left align-middle transition-all`}
              >
                {title ? (
                  <div className="text-lg mb-3 font-semibold text-white">
                    {title}
                  </div>
                ) : null}
                {children}
              </Dialog.Panel>
              {action && (
                <>
                  <button onClick={onClose}>{cancelText}</button>
                  <button onClick={onClose}>{comfirmText}</button>
                </>
              )}
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition>
  );
};

export default Modal;
