import { IconMoon, IconSearch, IconSun } from "@/assets";
import CustomButton from "@/components/button";
import Modal from "@/components/modal";
import React, { useEffect, useState } from "react";
import { LiaSearchSolid } from "react-icons/lia";

const Action = () => {
  const [darkMode, setDarkMode] = useState(false);
  const [openModalSearch, setOpenModalSearch] = useState(false);

  useEffect(() => {
    if (localStorage.getItem("darkMode") === "true") {
      setDarkMode(true);
    }
  }, []);

  useEffect(() => {
    if (darkMode) {
      document.documentElement.classList.add("dark");
      localStorage.setItem("darkMode", "true");
    } else {
      document.documentElement.classList.remove("dark");
      localStorage.setItem("darkMode", "false");
    }
  }, [darkMode]);

  const toggleDarkMode = () => {
    setDarkMode(!darkMode);
  };
  return (
    <div className="flex items-center">
      <div
        className="w-10 h-10 md:h-12 md:w-12 hover:bg-bgIconHover rounded-full dark:hover:bg-bgIconHoverDark flex items-center justify-center cursor-pointer select-none"
        onClick={toggleDarkMode}
      >
        {!darkMode ? <IconSun /> : <IconMoon />}
      </div>
      <div
        onClick={() => setOpenModalSearch(true)}
        className="hidden h-12 w-12 hover:bg-bgIconHover rounded-full dark:hover:bg-bgIconHoverDark md:flex items-center justify-center cursor-pointer select-none"
      >
        <IconSearch />
      </div>
      <CustomButton className="ml-3">Đăng nhập</CustomButton>
      <Modal
        isOpen={openModalSearch}
        onClose={() => setOpenModalSearch(false)}
        action={false}
        noBg
      >
        <div>
          <div className="flex items-center gap-x-2 py-5 px-4 bg-[rgb(55,65,81)] shadow-lg rounded-xl w-[512px] mt-20 text-sm">
            <LiaSearchSolid size={20} />
            <input
              type="text"
              className="flex-1 w-full h-full bg-transparent border-none outline-none"
              placeholder="Tìm bài viết"
            />
          </div>
        </div>
      </Modal>
    </div>
  );
};

export default Action;
