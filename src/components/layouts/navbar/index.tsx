import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";
import { listNav } from "./data.mock";

const Navbar = () => {
  const router = useRouter();
  return (
    <div className="hidden md:flex gap-x-2 items-center">
      {listNav.map((item, idx) => (
        <Link
          href={item.url}
          className={`px-[18px] py-2.5 rounded-full ${
            router.pathname === item.url
              ? "bg-bgIconHover dark:bg-bgIconHoverDark"
              : "hover:bg-bgIconHover/50 dark:hover:bg-bgIconHoverDark/50"
          }`}
          key={idx}
        >
          {item.name}
        </Link>
      ))}
    </div>
  );
};

export default Navbar;
