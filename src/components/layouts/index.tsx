import React, { ReactNode, useState } from "react";
import Header from "./header";
import NavFooter from "./nav-footer";
import SideBar from "./sidebar";

interface LayoutMainProps {
  children: ReactNode;
}

const LayoutMain = ({ children }: LayoutMainProps) => {
  const [isOpenSideBar, setIsOpenSideBar] = useState(false);
  return (
    <div className="dark:text-white">
      <Header />
      {children}
      <NavFooter setOpenSide={setIsOpenSideBar} />
      <SideBar isOpen={isOpenSideBar} onClose={() => setIsOpenSideBar(false)} />
    </div>
  );
};

export default LayoutMain;
