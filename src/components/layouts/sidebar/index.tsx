import { IconClose } from "@/assets";
import React from "react";
import { listNav } from "../navbar/data.mock";
import Link from "next/link";
import { useRouter } from "next/router";
import { listSocial } from "@/mocks/social";

interface SideBarProps {
  isOpen: boolean;
  onClose: any;
}

const SideBar = ({ isOpen, onClose }: SideBarProps) => {
  const router = useRouter();
  return (
    <div className="md:hidden">
      <div
        className={`fixed px-5 py-2 top-0 left-0 z-50 bg-white dark:bg-bgPrimaryDark w-[300px] h-screen transition-all duration-300 ${
          isOpen ? "translate-x-0" : "-translate-x-[300px]"
        }`}
      >
        <div className="flex items-center justify-between">
          <h1 className="text-primary text-xl md:text-2xl font-bold">Pure</h1>
          <div onClick={onClose}>
            {" "}
            <IconClose />
          </div>
        </div>
        <div className="pt-5 pb-3 flex flex-col font-medium border-b border-black/20 dark:border-white/20">
          {listNav.map((item, idx) => (
            <Link
              onClick={onClose}
              href={item.url}
              className={`w-full py-1 rounded-full ${
                router.pathname === item.url
                  ? "text-black dark:text-white"
                  : "text-black/70 dark:text-white/50"
              }`}
              key={idx}
            >
              {item.name}
            </Link>
          ))}
        </div>
        {/* Social */}
        {/* <div>
          {listSocial.map((item, idx) => (
            <Link href={item.url} key={idx}>
              {item.icon}
            </Link>
          ))}
        </div> */}
      </div>
      <div
        onClick={onClose}
        className={`bg-white/20 dark:bg-black/20 fixed inset-0 backdrop-blur-sm ${
          isOpen ? "block" : "hidden"
        }`}
      />
    </div>
  );
};

export default SideBar;
