import { IconHome, IconMenu, IconSearch, IconSearchMobile } from "@/assets";
import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";

interface NavFooterProps {
  setOpenSide: React.Dispatch<React.SetStateAction<boolean>>;
}

const NavFooter = ({ setOpenSide }: NavFooterProps) => {
  const list = [
    {
      name: "Home",
      icon: <IconHome />,
      url: "/",
    },
    {
      name: "Search",
      icon: <IconSearchMobile />,
      url: "/search",
    },
    {
      name: "Menu",
      icon: <IconMenu />,
    },
  ];

  const router = useRouter();
  const pathname = router.pathname;

  return (
    <div className="md:hidden fixed bottom-0 inset-x-0 ">
      <div className="w-full flex items-center justify-around border-t dark:border-neutral-700 border-neutral-300 p-2 h-[50px] bg-white dark:bg-[#1F2937] text-[10px] ">
        {list.map((item, idx) => (
          <div key={idx}>
            {item.url ? (
              <Link
                href={item.url}
                className={`flex flex-col items-center ${
                  pathname === item.url
                    ? "text-black dark:text-white "
                    : "text-black/70 dark:text-white/50"
                }`}
              >
                {item.icon}
                <p>{item.name}</p>
              </Link>
            ) : (
              <div
                onClick={() => setOpenSide(true)}
                className={`flex flex-col items-center ${
                  pathname === item.url
                    ? "text-black dark:text-white "
                    : "text-black/70 dark:text-white/50"
                }`}
              >
                {item.icon}
                {item.name}
              </div>
            )}
          </div>
        ))}
      </div>
    </div>
  );
};

export default NavFooter;
