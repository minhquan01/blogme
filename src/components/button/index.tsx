import React, { ButtonHTMLAttributes } from "react";

interface CustomButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {}

const CustomButton = ({ className, ...props }: CustomButtonProps) => {
  return (
    <button
      {...props}
      className={` text-white bg-primary py-2 px-4 md:py-3 md:px-6 md:font-medium rounded-full max-md:text-sm flex items-center justify-center ${className}`}
    >
      {props.children}
    </button>
  );
};

export default CustomButton;
