import Intro from "@/containers/me/intro";
import React from "react";

const Me = () => {
  return (
    <div>
      <Intro />
    </div>
  );
};

export default Me;
