import {
  IconFacebook,
  IconGitlab,
  IconLinkedin,
  IconMail,
  IconTiktok,
  IconYoutube,
} from "@/assets";

export const listSocial = [
  {
    icon: <IconFacebook />,
    url: "https://www.facebook.com/dangminhquan2001",
  },
  {
    icon: <IconLinkedin />,
    url: "https://www.linkedin.com/in/dangminhquan/",
  },
  {
    icon: <IconGitlab />,
    url: "https://gitlab.com/minhquan01",
  },
  {
    icon: <IconYoutube />,
    url: "https://www.youtube.com/channel/UC4meSAf7REvoPukxulr1pHg",
  },
  {
    icon: <IconTiktok />,
    url: "https://www.tiktok.com/@quandev01",
  },
  {
    icon: <IconMail />,
    url: "mailto:quandmk1@gmail.com",
  },
];
